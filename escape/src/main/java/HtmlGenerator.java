import com.floreysoft.jmte.Engine;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

public class HtmlGenerator {

    public static void main(String[] args) throws IOException {

        URL url = Resources.getResource("template.txt");
        String template = Resources.toString(url, Charsets.UTF_8);

        Engine engine = new Engine();
        Map<String, Object> model = new HashMap<>();
        model.put("title", escapeHtml4(args[0]));
        model.put("body",  escapeHtml4(args[1]));
        String output = engine.transform(template, model);
        System.out.println(output);

    }
}
