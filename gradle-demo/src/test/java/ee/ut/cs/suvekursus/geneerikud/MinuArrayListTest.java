package ee.ut.cs.suvekursus.geneerikud;

import static org.junit.Assert.*;

import org.junit.Test;

public class MinuArrayListTest {

	@Test
	public void testAddGetString() {
		MinuArrayList<String> list = new MinuArrayList<String>();
		list.add("tere");
		list.add("tore");
		assertEquals("tore", list.get(1));
		assertEquals("tere", list.get(0));
	}

	@Test
	public void testAddIntegers() {
		MinuArrayList<Integer> list = new MinuArrayList<Integer>();
		list.add(1);
		list.add(7);
		list.add(14);
		assertEquals(1, list.get(0).intValue());
		assertEquals(7, list.get(1).intValue());
		assertEquals(14, list.get(2).intValue());
	}


	@Test
	public void testAddManyIntegers() {
		MinuArrayList<Integer> list = new MinuArrayList<Integer>();
		for (int i = 0; i < 500; i++) {
			list.add(i);
		}
		assertEquals(5, list.get(5).intValue());
		assertEquals(25, list.get(25).intValue());
		assertEquals(499, list.get(499).intValue());
	}

}
