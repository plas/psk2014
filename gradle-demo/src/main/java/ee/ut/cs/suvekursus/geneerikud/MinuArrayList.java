package ee.ut.cs.suvekursus.geneerikud;

import java.util.ArrayList;
import java.util.Arrays;

public class MinuArrayList<T> {

	int capacity = 16;
	int nextFreeSlot = 0;
	private Object[] elements = new Object[capacity];

	public void add(T element) {
		if (nextFreeSlot == capacity) {
            expandArray();
        }
		elements[nextFreeSlot++] = element;
	}

	private void expandArray() {
		capacity *= 2;
		elements = Arrays.copyOf(elements, capacity);
        nextFreeSlot--;
	}

	@SuppressWarnings("unchecked")
	public T get(int i) {
		return (T) elements[i];
	}
	
}
