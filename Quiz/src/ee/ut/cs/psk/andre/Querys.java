package ee.ut.cs.psk.andre;

import java.util.Arrays;
import java.util.List;

public class Querys {
	
	static String droptable = "DROP TABLE Question";
	
	static String create = "CREATE TABLE IF NOT EXISTS Question(ID INT auto_increment NOT NULL PRIMARY KEY, QuestionText TEXT, QuestionType VARCHAR(10) NOT NULL"
			+ ", QuestionAnswers TEXT, RightAnswer VARCHAR(100) NOT NULL)";

	
	static List<String> insert = Arrays.asList("INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ('Mis on Eesti pealinn?', 'MC', 'Tallinn/Tartu/Pärnu' ,'1')",
	"INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ('Mis on Läti pealinn?' , 'MC', 'Jelgava/Daugavpils/Riia' , '3')",
	"INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kes on Toomas Hendrik Ilves?' ,'SA' , 'Eesti president')",
	"INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kas eesti rahvuslill on rukkilill?', 'TF' ,'true')",
	"INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ( 'Mis on Leedu pealinn?','MC', 'Kaunas/Vilnius/Klaipeda' , '2')",
	"INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Mitu mulli on mullivees?','SA', '453')",
	"INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kas eesti rahvuslind on leevike?','TF', 'true')");
	

}
