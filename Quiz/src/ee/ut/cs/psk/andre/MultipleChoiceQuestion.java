package ee.ut.cs.psk.andre;

import java.util.List;

public class MultipleChoiceQuestion implements Question {

	private String questionText;
	private List<String> questionAnswers;
	private int correctAnswer;
	
	MultipleChoiceQuestion(String text, List<String> choices,
	        int correctAnswerIndex) {
		this.questionText = text;
		this.questionAnswers = choices;
		this.correctAnswer = correctAnswerIndex;
	}
	
	public String getQuestionText() {
		return questionText;
	}
	public List<String> getQuestionAnswers() {
		return questionAnswers;
	}

	public int getCorrectAnswer() {
		return correctAnswer;
	}

	@Override
	public String getHtml() {
		// TODO Auto-generated method stub
		return null;
	}

}
