package ee.ut.cs.psk.andre;

public class TrueFalseQuestion implements Question{
	private String questionText;
	private boolean correctAnswer;
	
	TrueFalseQuestion(String text, boolean correctAnswer) {
		 this.questionText = text;
		 this.correctAnswer = correctAnswer;
    }

	public boolean isCorrectAnswer() {
		return correctAnswer;
	}

	public String getQuestionText() {
		return questionText;
	}

	@Override
	public String getHtml() {
		// TODO Auto-generated method stub
		return null;
	}

}
