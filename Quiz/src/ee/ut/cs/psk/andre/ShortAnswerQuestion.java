package ee.ut.cs.psk.andre;

public class ShortAnswerQuestion implements Question {
	String questionText;
	String correctAnswer;
	
	ShortAnswerQuestion(String text, String correctAnswer) {
		this.questionText = text;
		this.correctAnswer = correctAnswer;
	}
	

	public String getQuestionText() {
		return questionText;
	}


	public String getCorrectAnswer() {
		return correctAnswer;
	}


	@Override
	public String getHtml() {
		// TODO Auto-generated method stub
		return null;
	}

}
