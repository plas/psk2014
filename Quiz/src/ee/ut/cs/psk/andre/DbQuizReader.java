package ee.ut.cs.psk.andre;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DbQuizReader {
	// JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.h2.Driver"; //org.h2.Driver
    static final String DB_URL = "jdbc:h2:./test";

    //  Database credentials
    static final String USER = "sa";
    static final String PASS = "";
    
    public static List<Question> Read() { 
    	Connection conn = null;
        Statement stmt = null;
        List<Question> questions = new ArrayList<Question>();

        try {
            //Register JDBC driver
            Class.forName("org.h2.Driver");

            //Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
            
            //check if you have Questions table in database
            stmt.execute(Querys.droptable);
            stmt.execute(Querys.create);
            
            for (int i = 0; i < Querys.insert.size(); i++) {
				stmt.executeUpdate(Querys.insert.get(i));
			}
            
            //Starting to read data
            try {
				ResultSet rs = stmt.executeQuery("Select questionText, questionType, "
						+ "questionAnswers, rightAnswer FROM Question");
				while (rs.next()) {
					if (rs.getString(2).equals("MC")) { 
						questions.add(new MultipleChoiceQuestion(rs.getString(1),
								Arrays.asList(rs.getString(3).split("/")), 
								Integer.parseInt(rs.getString(4))));
					}
					else if (rs.getString(2).equals("TF")) {
						questions.add(new TrueFalseQuestion( rs.getString(1), 
								Boolean.parseBoolean(rs.getString(4))));
					} else if (rs.getString(2).equals("SA")) {
						questions.add(new ShortAnswerQuestion(rs.getString(1), rs.getString(4)));
					} else {
						System.out.println("Unvalid type here : " + rs.getString(2) +
								" and question is: " + rs.getString(1));
					}
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
            // Closing connection
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        System.out.println("All went well here! ");
        return questions;
    }
    
}
