package ee.ut.cs.psk.andre;

public interface Question {
	public String getHtml();
	public String getQuestionText();
}
