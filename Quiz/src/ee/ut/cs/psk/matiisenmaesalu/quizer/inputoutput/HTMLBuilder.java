package ee.ut.cs.psk.matiisenmaesalu.quizer.inputoutput;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.instrument.IllegalClassFormatException;
import java.util.List;

import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.ChoiceQuestion;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.InputQuestion;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.Question;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.YesNoQuestion;

/**
 * HTML Builder class handles the quiz' web page creation.
 * @author Ants-Oskar M�esalu
 */
public class HTMLBuilder {
	String output;
	
	/**
	 * Constructor with the appropriate question list.
	 * @param questions
	 * @throws IllegalClassFormatException
	 */
	public HTMLBuilder(String quizName, List<Question> questions) throws IllegalClassFormatException {
		buildHeader(quizName);
		buildForm(questions);
		buildFooter();
	}
	
	/**
	 * Builds the HTML file's header (everything before the form).
	 */
	private void buildHeader(String quizName) {
		output = "<html>\r\n"
				+ "<head>\r\n"
				+ "<meta charset=\"UTF-8\">\r\n"
				+ "<title>" + quizName + "</title>\r\n"
				+ "</head>\r\n"
				+ "<body>\r\n"
				+ "<h1>" + quizName + "</h1>\r\n";
	}
	
	/**
	 * Builds the HTML file's footer (everything after the form).
	 */
	private void buildFooter() {
		output += "</body>\r\n"
				+ "</html>";
	}
	
	/**
	 * Builds the form on the HTML page.
	 * @param questions
	 * @throws IllegalClassFormatException
	 */
	private void buildForm(List<Question> questions) throws IllegalClassFormatException {
		output += "<form action=\"mailto:someone@example.com\" method=\"post\" enctype=\"text/plain\">\r\n";
		for (int i = 0; i < questions.size(); i++) {
			output += "<h2>" + questions.get(i).getQuestion() + "</h2>\r\n";
			if (questions.get(i).getClass() == ChoiceQuestion.class) {
				buildChoiceQuestion((ChoiceQuestion) questions.get(i), i);
			} else if (questions.get(i).getClass() == InputQuestion.class) {
				buildInputQuestion((InputQuestion) questions.get(i), i);
			} else if (questions.get(i).getClass() == YesNoQuestion.class) {
				buildYesNoQuestion((YesNoQuestion) questions.get(i), i);
			} else {
				throw new IllegalClassFormatException("The program stumbled upon an illegal question type.");
			}
		}
		output += "<br/>\r\n"
				+ "<input type=\"submit\" value=\"Saada vastused\" />\r\n"
				+ "</form>\r\n";
	}
	
	/**
	 * Builds the HTML appropriate for a multiple choice question.
	 * @param question
	 * @param index
	 */
	private void buildChoiceQuestion(ChoiceQuestion question, int index) {
		output += "<select name=\"kysimus" + index + "\">\r\n";
		for (int i = 0; i < question.getNumberOfChoices(); i++) {
			output += "<option>" + question.getChoice(i) + "</option>\r\n";
		}
		output += "</select>\r\n";
	}
	
	/**
	 * Builds the HTML appropriate for a question awaiting user input.
	 * @param question
	 * @param index
	 */
	private void buildInputQuestion(Question question, int index) {
		output += "<input name=\"kysimus" + index + "\" />\r\n";
	}
	
	/**
	 * Builds the HTML appropriate for a boolean logic question.
	 * @param question
	 * @param index
	 */
	private void buildYesNoQuestion(Question question, int index) {
		output += "<input type=\"radio\" name=\"kysimus" + index + "\" value=\"jah\">\r\n"
				+ "<input type=\"radio\" name=\"kysimus" + index + "\" value=\"ei\">\r\n";
	}
	
	/**
	 * Saves the constructed HTML to a file with the specified name.
	 * @param fileName
	 * @throws IOException 
	 */
	public void save(String filePath) throws IOException {
		File file = new File(filePath);
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"));
		writer.append(output);
		writer.flush();
		writer.close();
	}
}
