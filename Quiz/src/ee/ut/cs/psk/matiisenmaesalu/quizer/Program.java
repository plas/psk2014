package ee.ut.cs.psk.matiisenmaesalu.quizer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;

import ee.ut.cs.psk.matiisenmaesalu.quizer.inputoutput.HTMLBuilder;
import ee.ut.cs.psk.matiisenmaesalu.quizer.inputoutput.QuizInput;

/**
 * The main class of the program.
 * @author Ott Matiisen
 * @author Ants-Oskar M�esalu
 */
public class Program {
	
	/**
	 * The main method of the program.
	 * If the program receives one argument on execution, the interactive mode of the quiz is activated.
	 * If the program receives two arguments on execution, the quiz is saved to the corresponding file.
	 * @param args
	 * @throws IllegalClassFormatException
	 * @throws IOException
	 */
	public static void main(String[] args) throws IllegalClassFormatException, IOException {
		if (args.length == 1) {
			Quiz quiz = QuizInput.readFromFileInput(args[0]);
			// TODO: Interaktiivne k�simustik
		} else if (args.length == 2) {
			Quiz quiz = QuizInput.readFromFileInput(args[0]);
			HTMLBuilder output = new HTMLBuilder(quiz.getName(), quiz.getQuestions());
			output.save(args[1]);
		} else {
			throw new IllegalArgumentException("Either 1 or 2 arguments have to be provided on execution.");
		}
	}
}
