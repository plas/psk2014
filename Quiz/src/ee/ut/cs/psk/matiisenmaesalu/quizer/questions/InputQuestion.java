package ee.ut.cs.psk.matiisenmaesalu.quizer.questions;

/**
 * Question class with short user text input as the answer.
 * @author Ants-Oskar M�esalu
 */
public class InputQuestion extends Question {
	String correctAnswer;
	String answer;

	/**
	 * Constructor with the question string and the correct answer (string).
	 * @param question
	 * @param correctAnswer
	 */
	public InputQuestion(String question, String correctAnswer) {
		super(question);
		this.correctAnswer = correctAnswer;
	}
	
	/**
	 * Return the correct answer.
	 * @return
	 */
	public String getCorrectAnswer() {
		return correctAnswer;
	}
	
	/**
	 * Return the user's answer.
	 * @return
	 */
	public String getAnswer() {
		return answer;
	}
	
	/**
	 * Answer the question. Method is used by the user.
	 * @param answer
	 * @return
	 */
	public boolean answer(String answer) {
		this.answer = answer;
		return answer.equals(correctAnswer);
	}
}
