package ee.ut.cs.psk.matiisenmaesalu.quizer.questions;

/**
 * Primary question class. All of the questions in the form are inherited from this question.
 * @author Ants-Oskar M�esalu
 */
public abstract class Question {
	String question;
	
	/**
	 * Constructor with the question string as it's parameter.
	 * @param question
	 */
	public Question(String question) {
		this.question = question;
	}
	
	/**
	 * Return the question string.
	 * @return
	 */
	public String getQuestion() {
		return question;
	}
}
