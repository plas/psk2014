package ee.ut.cs.psk.matiisenmaesalu.quizer.inputoutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import ee.ut.cs.psk.matiisenmaesalu.quizer.Quiz;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.ChoiceQuestion;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.InputQuestion;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.Question;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.YesNoQuestion;

/**
 * Everything from the .txt will be formed into a Quiz - list of Questions - and
 * returns it
 * 
 * @author Ott Matiisen
 * @param filePath
 * @return
 */
public class QuizInput {

	public static Quiz readFromFileInput(String filePath)
			throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(filePath));
		Quiz quiz = new Quiz(new ArrayList<Question>());
		String line = scanner.nextLine();
		while (!line.isEmpty()) {
			String[] parts = line.split(" ; ");
			if (parts.length == 2) {
				Question question = new InputQuestion(parts[0], parts[1]);
				quiz.getQuestions().add(question);
			} else if (parts[1].isEmpty()) {
				Question question = new YesNoQuestion(parts[0], parts[2]);
				quiz.getQuestions().add(question);
			} else {
				String[] split_answers = parts[1].split("/");
				ArrayList<String> answers = new ArrayList<String>();
				for (String string : split_answers) {
					answers.add(string);
				}
				Question question = new ChoiceQuestion(parts[0], answers,
						Integer.parseInt(parts[2]));
				quiz.getQuestions().add(question);
			}
		}
		scanner.close();
		return quiz;
	}

	public static void fromFileToDatabase(String filePath) throws SQLException,
			FileNotFoundException {

		String typeNr = "0";
		boolean binary = false;
		
		Connection connection = DriverManager
				.getConnection("jdbc:sqlanywhere:uid=dba;pwd=sql;"
						+ "Dbf=C:\\Users\\Ott\\My Real Documents\\Databases\\Quiz.db");
		PreparedStatement newQuiz = connection
				.prepareStatement("INSERT INTO Quiz (Name) VALUES (" + filePath
						+ ")");
		ResultSet addQuiz = newQuiz.executeQuery();
		newQuiz.close();
		addQuiz.close();

		PreparedStatement quizNr = connection
				.prepareStatement("SELECT Id FROM Quiz WHERE Name = '"
						+ filePath + "'");
		ResultSet getQuizNr = quizNr.executeQuery();
		String quizId = getQuizNr.toString();
		quizNr.close();
		getQuizNr.close();

		Scanner scanner = new Scanner(new File(filePath));
		String line = scanner.nextLine();
		while (!line.isEmpty()) {
			String[] parts = line.split(" ; ");
			String question = parts[0];
			// InputQuestion
			if (parts.length == 2) {
				typeNr = "3";
				QuizInput.answerIntoDatabase(connection, question, parts[1], binary);
				// YesNoQuestion
			} else if (parts[1].isEmpty()) {
				typeNr = "2";
				String[] answers = {"jah", "ei"};
				for (String answer : answers) {
					if(answer.equals(parts[2])) {
						binary = true;
					}
					QuizInput.answerIntoDatabase(connection, question, answer, binary);
				}
				// ChoiceQuestion
			} else {
				typeNr = "1";
				String[] answers = parts[1].split("/");
				for (int i = 0; i < answers.length; i++) {
					if(i + 1 == Integer.parseInt(parts[2])) {
						binary = true;
					}
					QuizInput.answerIntoDatabase(connection, question, answers[i], binary);
				}
			}
			QuizInput.questionIntoDatabase(connection, quizId, typeNr, question);
		}
		scanner.close();
	}

	public static void questionIntoDatabase(Connection connection, String quizId,
			String typeNr, String question) throws SQLException {
		PreparedStatement newQuestion = connection
				.prepareStatement("INSERT INTO Question (QuizId, Question, QuestionType) VALUES ("
						+ quizId + ", '" + question + "', " + typeNr + ")");
		ResultSet insertQuestion = newQuestion.executeQuery();
		newQuestion.close();
		insertQuestion.close();
	}
	
	public static void answerIntoDatabase(Connection connection, String question,
			String answer, boolean binary) throws SQLException {
		
		PreparedStatement questionNr = connection
				.prepareStatement("SELECT Id FROM Question WHERE Question = '"
						+ question + "'");
		ResultSet getQuestionNr = questionNr.executeQuery();
		String questionId = getQuestionNr.toString();
		questionNr.close();
		getQuestionNr.close();	
		
		PreparedStatement newAnswer = connection
				.prepareStatement("INSERT INTO Question (QuestionId, Answer, Correct) VALUES ("
						+ questionId + ", '" + answer + "', " + binary + ")");
		ResultSet insertAnswer = newAnswer.executeQuery();
		newAnswer.close();
		insertAnswer.close();
	}
}
