package ee.ut.cs.psk.matiisenmaesalu.quizer.inputoutput;

import java.io.FileNotFoundException;
import java.sql.*;

import ee.ut.cs.psk.matiisenmaesalu.quizer.*;
import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.*;

public class DBInput {

	public static void readFromDatabase() throws SQLException {
		// �henduse attribuut Dbf viitab andmebaasi failile.
		// uid t�histab kasutajanime ja pwd parooli.
		Connection connection = DriverManager
				.getConnection("jdbc:sqlanywhere:uid=dba;pwd=sql;"
						+ "Dbf=C:\\Users\\Ott\\My Real Documents\\Databases\\Quiz.db");

		// PreparedStatement t��pi objektid esitavad SQL lauset andmebaasi jaoks
		// mugaval kujul
		PreparedStatement stmt = connection
				.prepareStatement("select Eesnimi, Perenimi from Isik");

		// ResultSet t��pi objektid esitavad p�ringu tulemusi
		ResultSet rs = stmt.executeQuery();

		// Meetod ResultSet#next liigutab tulemuses j�rjehoidjat edasi.
		while (rs.next()) {
			// ResultSet'i meetodid getString, getInt jt. v�imaldavad k�sida
			// fookuses oleva kirje erinevaid komponente.
			System.out.println(rs.getString("Eesnimi") + " "
					+ rs.getString("Perenimi"));
		}

		// Peale t�� l�petamist on soovitav objektid sulgeda,
		// see vabastab teatavad �henduse ja p�ringutega seotud andmebaasi
		// ressursid.
		rs.close();
		stmt.close();
		connection.close();
	}

	public static void readIntoDataase(String filePath) throws SQLException,
			FileNotFoundException {

		Quiz quiz = QuizInput.readFromFileInput(filePath);
		String typeNr = "0";
		Connection connection = DriverManager
				.getConnection("jdbc:sqlanywhere:uid=dba;pwd=sql;"
						+ "Dbf=C:\\Users\\Ott\\My Real Documents\\Databases\\Quiz.db");
		PreparedStatement newQuiz = connection
				.prepareStatement("INSERT INTO Quiz (Name) VALUES " + filePath);
		ResultSet addQuiz = newQuiz.executeQuery();
		newQuiz.close();
		addQuiz.close();

		PreparedStatement quizNr = connection
				.prepareStatement("SELECT Id FROM Quiz WHERE Name = '"
						+ filePath + "'");
		ResultSet getQuizNr = quizNr.executeQuery();
		int quizId = Integer.parseInt(getQuizNr.toString());
		quizNr.close();
		getQuizNr.close();

		for (int i = 0; i < quiz.getQuestions().size(); i++) {
			if (quiz.getQuestions().get(i) instanceof ChoiceQuestion) {
				typeNr = "1";
			} else if (quiz.getQuestions().get(i) instanceof YesNoQuestion) {
				typeNr = "2";
			} else if (quiz.getQuestions().get(i) instanceof InputQuestion) {
				typeNr = "3";
			}
			PreparedStatement newQuestion = connection
					.prepareStatement("INSERT INTO Question (QuizId, Question, QuestionType) VALUES ("
							+ quizId
							+ ", '"
							+ quiz.getQuestions().get(i)
							+ "', '"
							+ typeNr
							+")");
			ResultSet insertQuestion = newQuestion.executeQuery();
			newQuestion.close();
			insertQuestion.close();
			for (int j = 0; j < quiz.getQuestions().size(); j++) {
				PreparedStatement newAnswer = connection.prepareStatement("INSERT INTO Answer");			
			}

		}
		connection.close();
	}
}
