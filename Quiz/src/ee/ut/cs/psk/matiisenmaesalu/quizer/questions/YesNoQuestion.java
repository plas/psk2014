package ee.ut.cs.psk.matiisenmaesalu.quizer.questions;

/**
 * Question class with a yes or no boolean type answer.
 * @author Ants-Oskar M�esalu
 */
public class YesNoQuestion extends Question {
	boolean correctAnswer;
	boolean answer;
	
	/**
	 * Constructor with the question string and the correct answer as a boolean type.
	 * @param question
	 * @param correctAnswer
	 */
	public YesNoQuestion(String question, String correctAnswer) {
		super(question);
		this.correctAnswer = (correctAnswer.toLowerCase().equals("jah"));
	}
	
	/**
	 * Return the correct answer to the question.
	 * @return
	 */
	public boolean getCorrectAnswer() {
		return correctAnswer;
	}
	
	/**
	 * Return the user's answer to the question.
	 * @return
	 */
	public boolean getAnswer() {
		return answer;
	}
	
	/**
	 * Answer the question. Method is used by the user.
	 * @param answer
	 * @return
	 */
	public boolean answer(boolean answer) {
		this.answer = answer;
		return answer == correctAnswer;
	}
}
