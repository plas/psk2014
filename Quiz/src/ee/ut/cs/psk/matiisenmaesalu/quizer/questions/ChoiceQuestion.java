package ee.ut.cs.psk.matiisenmaesalu.quizer.questions;

import java.util.List;

/**
 * Question class with choices as the answers.
 * @author Ants-Oskar M�esalu
 */
public class ChoiceQuestion extends Question {
	List<String> choices;
	Integer correctAnswerId;
	Integer answerId;
	
	/**
	 * Constructor with the question string, choice string list and the correct answer's id number in the list.
	 * @param question
	 * @param choices
	 * @param correctAnswerId
	 */
	public ChoiceQuestion(String question, List<String> choices, Integer correctAnswerId) {
		super(question);
		this.choices = choices;
		if (getNumberOfChoices() > correctAnswerId && correctAnswerId >= 0) {
			this.correctAnswerId = correctAnswerId;
		} else {
			throw new StackOverflowError("You tried to enter an illegal id as the correct answer.");
		}
	}
	
	/**
	 * Return the string list of the choices.
	 * @return
	 */
	public List<String> getChoices() {
		return choices;
	}
	
	/**
	 * Return the number of choices for the question.
	 * @return
	 */
	public Integer getNumberOfChoices() {
		return choices.size();
	}
	
	/**
	 * Return the choice string at the selected index.
	 * @param index
	 * @return
	 */
	public String getChoice(int index) {
		if (getNumberOfChoices() > index && index >= 0) {
			return choices.get(index);
		} else {
			throw new StackOverflowError("You tried to access an illegal choice index.");
		}
	}
	
	/**
	 * Return the correct answer's id number.
	 * @return
	 */
	public Integer getCorrectAnswerId() {
		return correctAnswerId;
	}
	
	/**
	 * Return the correct answer string.
	 * @return
	 */
	public String getCorrectAnswer() {
		return choices.get(correctAnswerId);
	}
	
	/**
	 * Return the user's answer's id number.
	 * @return
	 */
	public Integer getAnswerId() {
		return answerId;
	}
	
	/**
	 * Return the user's answer string.
	 * @return
	 */
	public String getAnswer() {
		if (answerId != null) {
			return choices.get(answerId);
		} else {
			throw new NullPointerException("The user's answer has not yet been set.");
		}
	}
	
	/**
	 * Answer the question. Method is used by the user.
	 * @param answerId
	 * @return
	 */
	public boolean answer(Integer answerId) {
		this.answerId = answerId;
		return answerId.equals(correctAnswerId);
	}
}
