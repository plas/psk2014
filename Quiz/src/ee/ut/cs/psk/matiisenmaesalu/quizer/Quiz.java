package ee.ut.cs.psk.matiisenmaesalu.quizer;
import java.util.List;

import ee.ut.cs.psk.matiisenmaesalu.quizer.questions.Question;

/**
 * The class for managing a quiz.
 * @author Ott Matiisen
 * @author Ants-Oskar M�esalu
 */
public class Quiz {
	private String name;
	private List<Question> questions;

	public Quiz(String name, List<Question> questions) {
		super();
		this.name = name;
		this.questions = questions;
	}

	public Quiz(List<Question> questions) {
		super();
		this.questions = questions;
	}

	public Quiz() {
		super();
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuiz(List<Question> questions) {
		this.questions = questions;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Quiz [name=" + name + ", questions=" + questions + "]";
	}
}
