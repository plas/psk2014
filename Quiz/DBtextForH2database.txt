DROP TABLE QUESTION;
CREATE TABLE Question(ID INT auto_increment NOT NULL PRIMARY KEY, QuestionText TEXT, QuestionType VARCHAR(10) NOT NULL, QuestionAnswers TEXT, RightAnswer VARCHAR(100) NOT NULL);

INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ('Mis on Eesti pealinn?', 'MC', 'Tallinn/Tartu/P�rnu' ,'1');
INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ('Mis on L�ti pealinn?' , 'MC', 'Jelgava/Daugavpils/Riia' , '3');
INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kes on Toomas Hendrik Ilves?' ,'SA' , 'Eesti president');
INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kas eesti rahvuslill on rukkilill?', 'TF' ,'true');
INSERT INTO QUESTION(questionText, questionType, questionAnswers, rightAnswer) VALUES ( 'Mis on Leedu pealinn?','MC', 'Kaunas/Vilnius/Klaipeda' , '2');
INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Mitu mulli on mullivees?','SA', '453');
INSERT INTO QUESTION(questionText, questionType, rightAnswer) VALUES ('Kas eesti rahvuslind on leevike?','TF', 'true');

SELECT * FROM Question;

// Database has a table Question, it has fields ID, questionText, questionType, rightAnswer, but
// we don't care about ID right now. Questiontypes are 'MC' for multiple choice questions, 
// 'TF' for truth false questions, and 'SA' for short answer questions. Also if 'TF' there are answers
// true or false, not jah or ei or something else.